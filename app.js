var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var irc = require('irc');
var path = require('path');
var uniqid = require('uniqid');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));

var proxies = {};

/**
 * Retorna um objeto do tipo proxy
 * @param {*} server 
 * @param {*} nick 
 * @param {*} channel 
 * @param {*} socket 
 */
function createProxy(id, server, nick, channel) {

	var client = new irc.Client(server, nick, { channels: [channel] });

	client.addListener("message" + channel, function(from, message) {

		var proxy = proxies[id];
		
		if (proxy.socket) {
			proxy.socket.emit("chat", message, from);
		}

	});

	client.addListener("nick", function(oldnick, newnick, channels, message) {
		proxies[id].socket.emit("update", oldnick + " mudou seu nickname para " + newnick);
	});

	client.addListener("quit", function(nick, reason, channels, message) {
		proxies[id].socket.emit("update", nick + " se desconectou");
	});

	client.addListener("error", function(message) {
		proxies[id].socket.emit("command-error", message.rawCommand + " " + message.command + ": " + message.args.pop());
	});

	proxies[id] = { id: id, client: client };
	return proxies[id];
}

/**
 * Executar uma funcionalidade do cliente IRC
 * @param {*} proxy 
 * @param {*} commands 
 */
function executeIrcCommand(proxy, commands) {

	var command = commands[0];
	commands.splice(0, 1);

	var message = commands.join(" ").trim();

	if (command == "/nick") {
		proxy.client.send("nick", message);
	
	} else if (command == "/quit") {
		proxy.client.send("quit", message);

	} else if (false) {
	}
}

/**
 * Setar funcionalidades do socket
 */
io.on("connection", function(socket) {

	socket.on("join", function(id) {

		var proxy = proxies[id];
		if (proxy && proxy.client) {

			proxy.socket = socket;
			
			// IRC Client
			var client = proxy.client;

			socket.emit("update", "Você se conectou!");
			socket.broadcast.emit("update", client.opt.nick + " se conectou");

			socket.on("send", function(message) {

				var commands = message.split(" ");
				var command = commands[0];

				if (command.charAt(0) == "/") {
					executeIrcCommand(proxy, commands);

				// Mensagem comum
				} else {
					client.say(client.opt.channels[0], message);
					socket.emit("chat", message);
				}

			});

			/* socket.on("disconnect", function() {
				io.emit("update", client.opt.nick + " se desconectou");
			}); */
		}

	});

});

/**
 * Rota inicial, direcionar para login ou inicializar chat em algum servidor
 */
app.get('/', function (req, res) {

	var server = req.cookies.server;
	var nick = req.cookies.nick;
	var channel = req.cookies.channel;

	if (server && nick && channel) {

		// Atribui o proxy a conexão
		var id = uniqid();
		var proxy = createProxy(id, server, nick, channel);

		res.cookie("id", id);
		res.sendFile(path.join(__dirname, '/index.html'));

	// Redirecionar para o login
	} else {
		res.sendFile(path.join(__dirname, '/login.html'));
	}

});

/**
 * Rota para login
 */
app.post("/login", function(req, res) {
	res.cookie("nick", req.body.nick);
	res.cookie("channel", req.body.channel);
	res.cookie("server", req.body.server);
	res.redirect("/");
});

http.listen(3000, function () {
	console.log('Example app listening on port 3000!');
});