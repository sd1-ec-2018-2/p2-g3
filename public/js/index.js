$(document).ready(function() {

    $(".page.chat").each(function() {

        var $self = $(this);
        var $chat = $(this).find(".chat-area");
        var $form = $(this).find(".chat-form");
        var $field = $form.find("[name=message]");
        var $sendButton = $form.find("button");

        var socket = io.connect("http://localhost:3000");
        socket.on("connect", function() {

            $field.removeAttr("disabled");
            $field.on("keyup", function() {
                
                if ($field.val()) {
                    $sendButton.removeAttr("disabled");
                } else {
                    $sendButton.attr("disabled", "disabled");
                }
    
            });
    
            $sendButton.on("click", function() {
                if ($field.val()) send();
            });
    
            $self.on("keypress", function(e) {
                if (e.which == 13 && $field.val()) send();
            });

            socket.emit("join", Cookies.get("id"));

            // Escrever mensagem de chat na tela
            socket.on("chat", function(message, from) {
                addMessage({ message: message, from: from });
            });

            // Escrever aviso no chat
            socket.on("update", function(message) {
                addMessage({ message: message, class: "info" });
            });

            socket.on("command-error", function(error) {
                addMessage({ message: error, class: "info danger" });
            });

        });

        /**
         * Adiciona uma mensagem na tela do chat
         * @param {*} data 
         */
        function addMessage(data) {

            var $li = $('<li><div>' + data.message + '</div></li>');
            var info = [ moment(new Date()).format("HH:mm") ];

            if (data.class) {
                $li.addClass(data.class);
            
            } else {

                if (data.from) {
                    info.unshift(data.from)
                
                } else {
                    $li.addClass("self");
                }
            }

            $li.find("div").prepend('<small>' + info.join(", ") + '</small>');
            $chat.find("ul").append($li);
            $chat.animate({ scrollTop: $chat.height() }, 1000);
        }

        /**
         * Função para enviar uma mensagem
         */
        var send = function() {
            socket.emit("send", $field.val()); $field.val("");
        }

    });

});